/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package core;

import ventana.Frame_Principal;

public class Main {

	public static void main(String[] args) {
		try {
			new Frame_Principal();
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Ha ocurrido un error!\n"
					+ "Porfavor cont�ctame para poder arreglarlo: stare@alumnos.ubiobio.cl � deathshadow_4@hotmail.com y dime qu� fue lo que intentaste hacer y sali� mal");
		}
	}
}
