/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class Frame_Alerta extends JFrame {

	// Atributos
	private static final int eWIDTH = 300;
	private static final int eHEIGHT = 150;
	// Componentes inicializados
	private JButton ok = null;
	private String mensaje = "	 ERROR";
	private JTextArea textArea = null;
	private JScrollPane textContainer = null;

	public Frame_Alerta(String m) {
		// Configuraci�n
		this.setVisible(true);
		this.setLayout(null);
		this.setTitle("ERROR");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setSize(eWIDTH, eHEIGHT);
		this.setPreferredSize(getSize());

		this.setMessage(m);

		// Componentes instanciados
		this.ok = new JButton("Aceptar");
		this.ok.setBounds(88, 85, 100, 30);
		this.ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				destroy();
			}
		});
		this.add(ok);
	}

	// M�todos
	public void setMessage(String m) {
		this.mensaje = m;
		this.textArea = new JTextArea();
		this.textArea.requestFocusInWindow();
		this.textArea.setEditable(false);
		this.textArea.setBounds(10, 10, 260, 50);
		this.textArea.setOpaque(false);
		this.textArea.setText(mensaje);
		this.textContainer = new JScrollPane(textArea);
		this.textContainer.setBounds(10, 10, 280, 70);
		this.add(textContainer);
	}

	// Solo para asegurar que este frame se oculte/destruya
	public void destroy() {
		this.dispose();
	}
}
