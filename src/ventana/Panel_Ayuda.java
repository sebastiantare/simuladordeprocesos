package ventana;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class Panel_Ayuda extends JPanel {

	private JTextArea textoAyuda = null;
	private JScrollPane scrollPane = null;
	private Font font = null;

	public Panel_Ayuda(Frame_Ayuda frame) {
		this.setSize(frame.getWidth(), frame.getHeight());
		this.setLayout(null);
		this.setOpaque(false);

		this.font = new Font("Verdana", Font.BOLD, 13);

		this.textoAyuda = new JTextArea();
		// Lo deje color plomo claro para que no le exploten los ojos a los
		// usuarios
		this.textoAyuda.setBackground(new Color(240, 240, 240));
		this.textoAyuda.setEditable(false);
		this.textoAyuda.setFont(this.font);
		this.scrollPane = new JScrollPane(this.textoAyuda);
		this.scrollPane.setBounds(10, 10, 750, 530);
		this.add(this.scrollPane);

		this.textoAyuda.setText("#################################################################\n\n"
				+ "Informaci�n sobre el programa:\n" + "\n" + "Este programa es un simulador de los algoritmos,\n"
				+ "el cual se encarga de seleccionar a qu� proceso \n"
				+ "se asigna el recurso procesador y durante cu�nto tiempo.\n" + "\n"
				+ "Todo sistema operativo gestiona los programas mediante el concepto de proceso.\n"
				+ "En un instante dado, en el ordenador pueden existir diversos procesos listos \n"
				+ "para ser ejecutados. \n"
				+ "Sin embargo, solamente uno de ellos puede ser ejecutado (en cada microprocesador).\n"
				+ "De ah� la necesidad de que una parte del sistema operativo gestione, de una manera equitativa,\n"
				+ "qu� proceso debe ejecutarse en cada momento para hacer un uso eficiente del procesador.\n" + "\n"
				+ "Por lo tanto, para motivo de aprendizaje, este programa sirve para conocer c�mo funcionan\n"
				+ "los algoritmos.\n\n"
				+ "A veces puede ocurrir que en los tiempos de espera media y retorno medio de valores negativos\n"
				+ "Pero no significa que est�n malos, sino que simplemente, el algoritmo no es el correcto\n"
				+ "para aplicarlo en dicho caso y por lo tanto el kernel deber� seleccionar otro.\n\n"
				+ "#################################################################\n\n" + "ALGORITMOS:\n"
				+ "1) FIFO (first in, first out)(� FCFS (first come, first served)): \n"
				+ "Este algoritmo es el m�s simple de todos los que est�n en este programa.\n"
				+ "Y tambi�n es el m�s f�cil de implementar, pero es ineficiente.\n"
				+ "Como su nombre lo indica, el primer proceso en llegar se le da todo el tiempo en CPU hasta\n"
				+ "que termine su ejecuci�n y por lo tanto es el primero en terminar o salir.\n" + "\n"
				+ "2) SJF(Short Job First) y SRTF (Short Remaining Time First):\n"
				+ "El algoritmo SJF se basa en elejir el proceso que se va a ejecutar en menor tiempo\n"
				+ "(el que va a ocupar menor tiempo en CPU), y se ejecuta hasta que termina, por lo tanto\n"
				+ "a diferencia de SRTF, se clasifica como 'No Expulsivo' (non-preemptive).\n"
				+ "En el caso de empate entre procesos con igual tiempo de CPU o r�faga de CPU\n"
				+ "se aplica FIFO, o por decirlo de otra forma, se selecciona el proceso\n"
				+ "que lleva m�s tiempo en espera (o que lleg� primero).\n"
				+ "El algoritmo SRTF es casi lo mismo que SJF, la �nica diferencia es que\n"
				+ "es Expulsivo (preemptive), esto quiere decir que, cuando un proceso est� ejecut�ndose,\n"
				+ "y llega otro con menor tiempo de CPU, se saca de ejecuci�n (se interrumpe al proceso) \n"
				+ "y se pone en espera para darle espacio al nuevo proceso entrante \n"
				+ "y el antiguo no se vuelve a ejecutar hasta que sea llamado nuevamente.\n" + "\n"
				+ "3) Prioridad (Expulsivo y No-Expulsivo):\n"
				+ "Se asocia a cada proceso un n�mero entero llamado prioridad de acuerdo con alg�n \n"
				+ "criterio asumiremos mayor prioridad con menor n�mero entero.\n"
				+ "A medida que los procesos van llegando listos para entrar a ejecuci�n,\n"
				+ "se selecciona el proceso con la prioridad m�s alta (Por ejemplo: 1)\n"
				+ "Y se ejecuta hasta que termina.\n"
				+ "Pero al contrario de la versi�n no expulsiva anteriormente mencionada\n"
				+ "El algoritmo de prioridad expulsivo interrumpe a el proceso en ejecuci�n\n"
				+ "si es que llega uno nuevo con mayor prioridad y lo deja en espera.\n" + "\n" + "4) Round Robin:\n"
				+ "Este algoritmo le asigna un intervalo de tiempo llamado Quantum\n"
				+ "el cual define por cuanto tiempo se ejecutar� ese proceso.\n"
				+ "Esto quiere decir que si llega un proceso con tiempo de ejecuci�n 3\n"
				+ "y tiene un quantum de 4, el proceso se ejecutar� sin ser interrumpido,\n"
				+ "pero si tiene un tiempo de ejecuci�n mayor, por ejemplo, de 6\n"
				+ "va a ser interrumpido en el tiempo 4 y dejado en espera hasta que\n"
				+ "vuelva a ser llamado a ejecuci�n, para que se ejecute en un tiempo de 2.\n\n"
				+ "#################################################################\n" + "\n"
				+ "Instrucciones de Uso:\n" + "1) En el panel de inicio selecciona un algoritmo a simular\n\n"
				+ "2) Ingresa un valor entero positivo en el campo de texto en la esquina\n"
				+ "izquierda superior para indicarle al programa cuantos procesos usar�s\n"
				+ "y dale click al bot�n Generar Tabla para que aparezca la tabla de procesos.\n\n"
				+ "3) Llena los campos correctamente. Esto significa que en los campos de las columnas\n"
				+ "Tiempo de llegada, Tiempo de r�faga y Prioridad deber�s ingresar un n�mero entero positivo\n"
				+ "seg�n corresponda a cada algoritmo.\n"
				+ "4) Si quieres simular el algoritmo Round Robin, ingresa un n�mero entero positivo\n"
				+ "en el campo de Quantum\n\n"
				+ "5) Presiona el bot�n calcular. Si ingresaste los datos correctamente se dibujar�\n"
				+ "el diagrama de Gantt y se calcular�n los tiempos medio de llegada y retorno.\n\n"
				+ "#################################################################\n" + "\n" + "CONTACTO:\n"
				+ "Cualquier duda sobre el programa puedes pregunt�rmela via email:\n" + "stare@alumnos.ubiobio.cl\n"
				+ "deathshadow_4@hotmail.com");
		this.textoAyuda.setCaretPosition(0);
	}
}
