/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package ventana;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class Panel_RR extends JPanel {

	// Componentes inicializados
	private JButton calcular = null;
	private JButton volver = null;

	private JLabel nfilas = null;
	private JTextField filas = null;
	private JButton generar = null;

	private JLabel labelprocesos = null;
	private JTable procesos = null;
	private JScrollPane tableContainer = null;
	private Vector<String> columnNames = null;
	private Vector<Object> rowData = null;

	private JLabel labelgantt = null;
	private JTextArea gantt = null;
	private JScrollPane textContainer = null;

	private JLabel esperaMed = null;
	private JLabel retornoMed = null;

	private Font font = null;

	private Vector<String> nombres = null;
	private Vector<Integer> rafaga = null;
	private Vector<Integer> position = null;
	private Vector<Integer> rr = null;
	private Vector<Integer> ganttValues = null;
	private Vector<Integer> processOrder = null;
	private int quantum = 0;

	private JLabel resultEspera = null;
	private JLabel resultRetorno = null;

	private JLabel labelQuantum = null;
	private JTextField valueQuantum = null;

	// Constructor
	public Panel_RR(Frame_Principal frame) {

		// Configuraci�n
		this.setVisible(false);
		this.setSize(frame.getWIDTH(), frame.getHEIGHT());
		this.setLayout(null);
		this.setOpaque(false);

		// Componentes instanciados
		this.labelprocesos = new JLabel("Tabla de Procesos");
		this.labelprocesos.setBounds((int) (frame.getWIDTH() * 0.8) / 2, 10, 200, 25);
		this.add(this.labelprocesos);

		this.columnNames = new Vector<>();
		this.rowData = new Vector<>();
		this.rafaga = new Vector<>();
		this.nombres = new Vector<>();
		this.position = new Vector<>();
		this.ganttValues = new Vector<>();
		this.processOrder = new Vector<>();
		this.rr = new Vector<>();

		this.columnNames.add("Proceso");
		this.columnNames.add("Tiempo de R�faga");

		this.procesos = new JTable(rowData, columnNames);
		this.procesos.getTableHeader().setReorderingAllowed(false);
		this.tableContainer = new JScrollPane(this.procesos);
		this.tableContainer.setBounds(10, 30, (int) (frame.getWIDTH() * 0.8), (frame.getHEIGHT() / 2) - 20);
		this.add(this.tableContainer);

		this.nfilas = new JLabel("N de Procesos");
		this.nfilas.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 10, 100, 30);
		this.add(nfilas);

		this.filas = new JTextField();
		this.filas.setBounds((int) (frame.getWIDTH() * 0.8) + 110, 10, 40, 25);
		this.add(this.filas);

		this.generar = new JButton("Generar Tabla");
		this.generar.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 40, 140, 25);
		this.generar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					if (Integer.parseInt(filas.getText()) < 1 || filas.getText() == null) {
						new Frame_Alerta(
								"En el campo de numero de filas" + "\nDebe ingresar un numero entero positivo");
						return;
					}

					int newRows = Integer.parseInt(filas.getText());
					setProcessTable(newRows);

				} catch (Exception e2) {
					// e2.printStackTrace();
					// e2.getMessage();
					new Frame_Alerta("Ingrese un numero entero positivo para generar tabla");
				}
			}
		});
		this.add(this.generar);

		this.labelQuantum = new JLabel("Quantum");
		this.labelQuantum.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 80, 100, 25);
		this.add(this.labelQuantum);

		this.valueQuantum = new JTextField();
		this.valueQuantum.setBounds((int) (frame.getWIDTH() * 0.8) + 110, 80, 40, 25);
		this.add(this.valueQuantum);

		this.resultEspera = new JLabel("0");
		this.resultEspera.setBounds(10 + 200, (int) (frame.getHEIGHT() / 1.8) + 100, 200, 30);
		this.add(this.resultEspera);

		this.resultRetorno = new JLabel("0");
		this.resultRetorno.setBounds(10 + 200, (int) (frame.getHEIGHT() / 1.8) + 150, 200, 30);
		this.add(this.resultRetorno);

		this.calcular = new JButton("Calcular");
		this.calcular.setBounds((int) (frame.getWIDTH() * 0.8) + 10, (frame.getHEIGHT() / 2) - 41, 140, 50);
		this.calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Manejo de excepciones
				try {
					if (procesos.getRowCount() < 1) {
						new Frame_Alerta("Antes de calcular \ndebe generar una tabla e ingresar los datos");
						return;
					}

					if (valueQuantum.getText().length() < 1 || valueQuantum == null) {
						new Frame_Alerta("Debe ingresar un numero entero positivo en el Quantum");
						return;
					}

					if (Integer.parseInt(valueQuantum.getText()) < 1) {
						new Frame_Alerta("Debe ingresar un quantum mayor a 0");
						return;
					}

					for (int i = 0; i < procesos.getRowCount(); i++) {
						if (Integer.parseInt("" + procesos.getValueAt(i, 1)) < 0) {
							new Frame_Alerta("No est� permitido que se ingresen\n" + "valores negativos");
							return;
						}
					}
				} catch (Exception e3) {
					new Frame_Alerta("Debe ingresar los valores correctamente" + "\nS�lo valores enteros positivos"
							+ "\nA excepci�n de los nombres de los procesos");
					return;
				}

				// Algoritmo
				try {
					// Limpia los datos antiguos
					nombres.clear();
					rafaga.clear();
					ganttValues.clear();
					rr.clear();
					position.clear();

					quantum = Integer.parseInt("" + valueQuantum.getText());

					// Recolecta los datos de la tabla para transformarlos a int
					for (int j = 0; j < procesos.getRowCount(); j++) {
						nombres.add("" + procesos.getValueAt(j, 0));
						rafaga.add(Integer.parseInt("" + procesos.getValueAt(j, 1)));
					}

					// Copio los datos de las rafagas de cpu en otro vector
					for (int i = 0; i < rafaga.size(); i++) {
						rr.add(rafaga.get(i));
					}

					int contador = 0;

					ganttValues.add(contador);

					while (!checkVector(rr)) { // Mientras que el vector no est�
												// lleno de 0

						for (int j = 0; j < rafaga.size(); j++) {

							if (rr.get(j) <= quantum && rr.get(j) > 0) {
								contador += rr.get(j); // contador de rafagas de
														// cpu. Ejemplo:
														// (Quantum=2) 0 p1 2 p2
														// 3 p3 5...... contador
														// = 5
								ganttValues.add(contador); // almacena los
															// valores, ejemplo
															// anterior:
															// [0,2,3,5]
								rr.set(j, 0); // como el valor de rafaga es
												// menor o igual al quantum,
												// setea el valor del vector a 0
								processOrder.add(j); // almacena la posicion de
														// la tupla de la tabla
														// para sacar el nombre
								position.add(j);
							} else {
								if (rr.get(j) > quantum) {
									// Modifica valor en vector (valor -
									// quantum)
									contador += quantum;
									ganttValues.add(contador);
									rr.set(j, rr.get(j) - quantum);
									position.add(j);
								}
							}

						}
					} // end while

					// Dibujar diagrama
					String ganttText = "";

					for (int i = 0; i < ganttValues.size(); i++) {

						ganttText += ganttValues.get(i) + "   ";

						if (i < ganttValues.size() - 1) {
							ganttText += "[" + procesos.getValueAt(position.get(i), 0) + "]   ";
						}
					}

					gantt.setText(ganttText);

					// Espera media y retorno medio

					Vector<Integer> espera = new Vector<>();
					float esperaMedia = 0;
					float retornoMedio = 0;

					for (int i = 0; i < position.size(); i++) {
						for (int j = 0; j < position.size(); j++) {
							if (position.get(j) == i) {
								espera.add(ganttValues.get(j));
								espera.add(ganttValues.get(j + 1));
								// System.out.println("Valores " +
								// ganttValues.get(j) + " - " +
								// ganttValues.get(j + 1));
							}
						}
						for (int h = 0; h < espera.size(); h++) {
							if (h == 0) {
								esperaMedia += espera.get(h);
								retornoMedio += espera.get(espera.size() - 1);
							} else {
								esperaMedia += (espera.get(h) - espera.get(h - 1));
							}
							h++; // Para que se salte 1 numero
						}
						espera.clear();
					}

					esperaMedia = esperaMedia / rafaga.size();
					retornoMedio = retornoMedio / rafaga.size();

					resultEspera.setText("" + esperaMedia);
					resultRetorno.setText("" + retornoMedio);

					if ((retornoMedio < 0.0) || esperaMedia < 0.0) {
						new Frame_Alerta(
								"El algoritmo ha dado valores negativos\n" + "Esto quiere decir que este algoritmo\n"
										+ "no es aplicable para este caso\n" + "por lo tanto se debe elegir otro");
					}

				} catch (Exception e2) {
					// e2.printStackTrace();
					// e2.getMessage();
					new Frame_Alerta("Debe llenar los campos correctamente");
				}
			}
		});
		this.add(this.calcular);

		this.labelgantt = new JLabel("Diagrama de Gantt");
		this.labelgantt.setBounds((int) (frame.getWIDTH() * 0.8) / 2, (int) (frame.getHEIGHT() / 1.8) - 25, 200, 25);
		this.add(this.labelgantt);

		this.font = new Font("Verdana", Font.BOLD, 15);

		this.gantt = new JTextArea();
		this.gantt.setEditable(false);
		this.gantt.setFont(this.font);
		this.textContainer = new JScrollPane(this.gantt);
		this.textContainer.setBounds(10, (int) (frame.getHEIGHT() / 1.8), frame.getWIDTH() - 20, 100);
		this.add(this.textContainer);

		this.esperaMed = new JLabel("Tiempo medio de espera:");
		this.esperaMed.setBounds(10, (int) (frame.getHEIGHT() / 1.8) + 100, 200, 30);
		this.add(this.esperaMed);

		this.retornoMed = new JLabel("Tiempo medio de retorno:");
		this.retornoMed.setBounds(10, (int) (frame.getHEIGHT() / 1.8) + 150, 200, 30);
		this.add(this.retornoMed);

		this.volver = new JButton("Volver");
		this.volver.setBounds(1, frame.getHEIGHT() - 60, 100, 30);
		this.volver.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.pr.setVisible(false);
				frame.setTitle("Planificador de procesos");
				frame.pp.setVisible(true);
			}
		});
		this.add(this.volver);
	}

	// M�todos
	// Limpia y llena un vector con null para que los campos de la tabla est�n
	// vac�os
	private void setRowNumber(int rows) {
		this.rowData.clear();
		for (int i = 0; i < rows; i++) {
			this.rowData.addElement(null);
		}
	}

	// Limpia el modelo de la tabla (datos y columnas) y colca el nuevo
	private void setProcessTable(int rows) {
		this.setRowNumber(rows);
		this.procesos.setModel(new DefaultTableModel(this.rowData, this.columnNames));
		// Nombrar los procesos
		for (int i = 0; i < procesos.getRowCount(); i++) {
			procesos.setValueAt("P" + (i + 1), i, 0);
		}
	}

	// Revisa si los valores del vector son todos 0
	private boolean checkVector(Vector<Integer> v) {
		int count = 0;

		for (int i = 0; i < v.size(); i++) {
			if (v.get(i) == 0) {
				count++;
			}
		}

		if (count == v.size()) {
			return true;
		}

		return false;
	}
}
