/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package ventana;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class Panel_FIFO extends JPanel {

	// Componentes inicializados
	private JButton calcular = null;
	private JButton volver = null;

	private JLabel nfilas = null;
	private JTextField filas = null;
	private JButton generar = null;

	private JLabel labelprocesos = null;
	private JTable procesos = null;
	private JScrollPane tableContainer = null;
	private Vector<String> columnNames = null;
	private Vector<Object> rowData = null;

	private JLabel labelgantt = null;
	private JTextArea gantt = null;
	private JScrollPane textContainer = null;

	private JLabel esperaMed = null;
	private JLabel retornoMed = null;

	private Font font = null;

	private Vector<String> nombres = null;
	private Vector<Integer> llegada = null;
	private Vector<Integer> rafaga = null;
	private Vector<Integer> orden = null;
	private Vector<Integer> position = null;
	private Vector<Integer> ganttValues = null;

	private JTextArea resultEspera = null;
	private JTextArea resultRetorno = null;

	// Constructor
	public Panel_FIFO(Frame_Principal frame) {

		// Configuraci�n
		this.setVisible(false);
		this.setSize(frame.getWIDTH(), frame.getHEIGHT());
		this.setLayout(null);
		this.setOpaque(false);

		// Componentes instanciados
		this.labelprocesos = new JLabel("Tabla de Procesos");
		this.labelprocesos.setBounds((int) (frame.getWIDTH() * 0.8) / 2, 10, 200, 25);
		this.add(this.labelprocesos);

		this.columnNames = new Vector<>();
		this.rowData = new Vector<>();
		this.llegada = new Vector<>();
		this.rafaga = new Vector<>();
		this.nombres = new Vector<>();
		this.orden = new Vector<>();
		this.position = new Vector<>();
		this.ganttValues = new Vector<>();

		this.columnNames.add("Proceso");
		this.columnNames.add("Tiempo de Llegada");
		this.columnNames.add("Tiempo de R�faga");

		this.procesos = new JTable(rowData, columnNames);
		this.procesos.getTableHeader().setReorderingAllowed(false);
		this.tableContainer = new JScrollPane(this.procesos);
		this.tableContainer.setBounds(10, 30, (int) (frame.getWIDTH() * 0.8), (frame.getHEIGHT() / 2) - 20);
		this.add(this.tableContainer);

		this.nfilas = new JLabel("N de Procesos");
		this.nfilas.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 10, 100, 30);
		this.add(nfilas);

		this.filas = new JTextField();
		this.filas.setBounds((int) (frame.getWIDTH() * 0.8) + 110, 10, 40, 25);
		this.add(this.filas);

		this.generar = new JButton("Generar Tabla");
		this.generar.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 40, 140, 25);
		this.generar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					if (Integer.parseInt(filas.getText()) < 1 || filas.getText() == null) {
						new Frame_Alerta(
								"En el campo de numero de filas" + "\nDebe ingresar un numero entero positivo");
						return;
					}

					int newRows = Integer.parseInt(filas.getText());
					setProcessTable(newRows);

				} catch (Exception e2) {
					// e2.printStackTrace();
					// e2.getMessage();
					new Frame_Alerta("Ingrese un numero entero positivo para generar tabla");
				}
			}
		});
		this.add(this.generar);

		this.resultEspera = new JTextArea("");
		this.resultEspera.setBounds(10 + 180, (int) (frame.getHEIGHT() / 1.8) + 105, 500, 30);
		this.resultEspera.setBackground(null);
		this.add(this.resultEspera);

		this.resultRetorno = new JTextArea("");
		this.resultRetorno.setBounds(10 + 180, (int) (frame.getHEIGHT() / 1.8) + 155, 500, 30);
		this.resultRetorno.setBackground(null);
		this.add(this.resultRetorno);

		this.calcular = new JButton("Calcular");
		this.calcular.setBounds((int) (frame.getWIDTH() * 0.8) + 10, (frame.getHEIGHT() / 2) - 41, 140, 50);
		this.calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Manejo de excepciones
				if (procesos.getRowCount() < 1) {
					new Frame_Alerta("Antes de calcular \ndebe generar una tabla e ingresar los datos");
					return;
				}

				// Algoritmo
				try {
					
					for (int i = 0; i < procesos.getRowCount(); i++) {
						for (int j = 1; j < 3; j++) {
							if (Integer.parseInt("" + procesos.getValueAt(i, j)) < 0) {
								new Frame_Alerta("No est� permitido que se ingresen\n" + "valores negativos");
								return;
							}
						}
					}
					
					// Limpia los datos antiguos
					nombres.clear();
					llegada.clear();
					rafaga.clear();

					// Recolecta los datos de la tabla para transformarlos a int
					for (int j = 0; j < procesos.getRowCount(); j++) {
						nombres.add("" + procesos.getValueAt(j, 0));
						llegada.add(Integer.parseInt("" + procesos.getValueAt(j, 1)));
						rafaga.add(Integer.parseInt("" + procesos.getValueAt(j, 2)));
					}

					// Limpia las posiciones antiguas
					orden.clear();
					position.clear();

					//
					// Vector<Integer> pos = new Vector<>();
					int m = 0;
					// Calcular las posiciones por orden de llegada
					for (int x = 0; x < llegada.size(); x++) {

						// referencia para comparar
						for (int i = 0; i < llegada.size(); i++) {
							if ((orden.contains(llegada.get(i)) == false)) {
								m = llegada.get(i);
							}
						}

						// Valor minimo
						for (int i = 0; i < llegada.size(); i++) {
							if (llegada.get(i) < m && !orden.contains(llegada.get(i))) {
								m = llegada.get(i);
							}
						}

						// Posiciones de valores iguales al minimo
						for (int j = 0; j < llegada.size(); j++) {
							if (llegada.get(j) == m) {
								position.add(j);
							}
						}
						// Valores ya revisados
						orden.add(m);
					}

					// Diagrama de Gantt
					int acc = llegada.get(position.get(0));
					ganttValues.clear();
					ganttValues.add(acc);

					for (int i = 0; i < llegada.size(); i++) {
						acc += rafaga.get(position.get(i));
						ganttValues.add(acc);
					}

					// Dibujar diagrama
					String ganttText = "";

					for (int i = 0; i < ganttValues.size(); i++) {
						ganttText += ganttValues.get(i) + "   ";

						if (i < ganttValues.size() - 1) {
							ganttText += "[" + procesos.getValueAt(position.get(i), 0) + "]   ";
						}
					}

					gantt.setText(ganttText);

					// Calcular tiempo de espera medio
					float esperaMedio = 0;
					float retornoMedio = 0;
					String esperaMedi = "(";
					String retornoMedi = "";

					resultEspera.setText("");
					resultRetorno.setText("");

					for (int i = 0; i < llegada.size(); i++) {
						
						if (i == llegada.size() - 1) {
							esperaMedi += " " + nombres.get(position.get(i)) + "(" + ganttValues.get(i) + " - " + llegada.get(position.get(i))+")";
							esperaMedio += (float) ganttValues.get(position.get(i));
							esperaMedio -= (float) llegada.get(position.get(i));
							break;
						}
						
						esperaMedi += " " + nombres.get(position.get(i)) + "(" + ganttValues.get(i) + " - " + llegada.get(position.get(i))+") +";
						esperaMedio += (float) ganttValues.get(position.get(i));
						esperaMedio -= (float) llegada.get(position.get(i));
					}
					
					esperaMedio = esperaMedio / llegada.size();

					for (int i = 1; i < ganttValues.size(); i++) {
						
						if (i == ganttValues.size()) {
							retornoMedi += " " + nombres.get(position.get(i)) + "";
						}
						
						retornoMedio += (float) ganttValues.get(i);
					}

					retornoMedio = retornoMedio / llegada.size();

					resultEspera.setText(esperaMedi + ")/"+llegada.size()+" = " + esperaMedio);
					resultRetorno.setText("" + retornoMedio);

					if ((esperaMedio < 0.0) || retornoMedio < 0.0) {
						new Frame_Alerta(
								"El algoritmo ha dado valores negativos\n" + "Esto quiere decir que este algoritmo\n"
										+ "no es aplicable para este caso\n" + "por lo tanto se debe elegir otro");
					}

				} catch (Exception e2) {
					// e2.printStackTrace();
					// e2.getMessage();
					new Frame_Alerta("Debe llenar los campos correctamente");
				}
			}
		});
		this.add(this.calcular);

		this.labelgantt = new JLabel("Diagrama de Gantt");
		this.labelgantt.setBounds((int) (frame.getWIDTH() * 0.8) / 2, (int) (frame.getHEIGHT() / 1.8) - 25, 200, 25);
		this.add(this.labelgantt);

		this.font = new Font("Verdana", Font.BOLD, 15);

		this.gantt = new JTextArea();
		this.gantt.setEditable(false);
		this.gantt.setFont(this.font);
		this.textContainer = new JScrollPane(this.gantt);
		this.textContainer.setBounds(10, (int) (frame.getHEIGHT() / 1.8), frame.getWIDTH() - 20, 100);
		this.add(this.textContainer);

		this.esperaMed = new JLabel("Tiempo medio de espera:");
		this.esperaMed.setBounds(10, (int) (frame.getHEIGHT() / 1.8) + 100, 200, 30);
		this.add(this.esperaMed);

		this.retornoMed = new JLabel("Tiempo medio de retorno:");
		this.retornoMed.setBounds(10, (int) (frame.getHEIGHT() / 1.8) + 150, 200, 30);
		this.add(this.retornoMed);

		this.volver = new JButton("Volver");
		this.volver.setBounds(1, frame.getHEIGHT() - 60, 100, 30);
		this.volver.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.pf.setVisible(false);
				frame.setTitle("Planificador de procesos");
				frame.pp.setVisible(true);
			}
		});
		this.add(this.volver);
	}

	// M�todos
	// Limpia y llena un vector con null para que los campos de la tabla est�n
	// vac�os
	private void setRowNumber(int rows) {
		this.rowData.clear();
		for (int i = 0; i < rows; i++) {
			this.rowData.addElement(null);
		}
	}

	// Limpia el modelo de la tabla (datos y columnas) y colca el nuevo
	private void setProcessTable(int rows) {
		this.setRowNumber(rows);
		this.procesos.setModel(new DefaultTableModel(this.rowData, this.columnNames));
		// Nombrar los procesos
		for (int i = 0; i < procesos.getRowCount(); i++) {
			procesos.setValueAt("P" + (i + 1), i, 0);
		}
	}
}
