/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Panel_Principal extends JPanel {

	// Componentes inicializados
	private JButton FIFO = null;
	private JButton SJF = null;
	private JButton PRIOR = null;
	private JButton RR = null;
	private JLabel sebastianTare = null;
	private JButton ayuda = null;

	public Panel_Principal(Frame_Principal frame) {
		// Configuraci�n
		this.setSize(frame.getWIDTH(), frame.getHEIGHT());
		this.setLayout(null);
		this.setOpaque(false);

		// Componentes instanciados
		this.FIFO = new JButton("FIFO");
		this.FIFO.setBounds((frame.getWIDTH() / 2) - 100, frame.getHEIGHT() / 6, 200, 50);
		this.FIFO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.pp.setVisible(false);
				frame.setTitle("FIFO");
				frame.pf.setVisible(true);
			}
		});
		this.add(this.FIFO);

		this.SJF = new JButton("Short Job First");
		this.SJF.setBounds((frame.getWIDTH() / 2) - 100, (frame.getHEIGHT() / 6) + 100, 200, 50);
		this.SJF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.pp.setVisible(false);
				frame.setTitle("SJF Expulsivo y No-Expulsivo");
				frame.ps.setVisible(true);
			}
		});
		this.add(this.SJF);

		this.PRIOR = new JButton("Por Prioridad");
		this.PRIOR.setBounds((frame.getWIDTH() / 2) - 100, (frame.getHEIGHT() / 6) + 200, 200, 50);
		this.PRIOR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.pp.setVisible(false);
				frame.setTitle("Por Prioridad Expulsivo y No-Expulsivo");
				frame.prr.setVisible(true);
			}
		});
		this.add(this.PRIOR);

		this.RR = new JButton("Round Robin");
		this.RR.setBounds((frame.getWIDTH() / 2) - 100, (frame.getHEIGHT() / 6) + 300, 200, 50);
		this.RR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.pp.setVisible(false);
				frame.setTitle("Round Robin");
				frame.pr.setVisible(true);
			}
		});
		this.add(this.RR);

		this.sebastianTare = new JLabel("por Sebasti�n Tar� Bustos");
		this.sebastianTare.setBounds(640, 535, 200, 50);
		this.add(this.sebastianTare);

		this.ayuda = new JButton("Ayuda");
		this.ayuda.setBounds(1, 1, 100, 30);
		this.ayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Frame_Ayuda();
			}
		});
		this.add(this.ayuda);

		// Configuraci�n
		this.setVisible(true);
	}
}
