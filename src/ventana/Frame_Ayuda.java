package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Frame_Ayuda extends JFrame {

	// Atributos
	private static int WIDTH = 800;
	private static int HEIGHT = 600;

	// Componentes inicializados
	private Panel_Ayuda py = null;
	private JButton cerrar = null;

	// Constructor
	public Frame_Ayuda() {
		this.setVisible(true);
		this.setLayout(null);
		this.setTitle("Ayuda");
		this.setResizable(false);
		this.setSize(WIDTH, HEIGHT);

		this.py = new Panel_Ayuda(this);
		this.add(this.py);

		this.cerrar = new JButton("Cerrar");
		this.cerrar.setBounds(1, this.getHeight() - 60, 100, 30);
		this.cerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		this.add(this.cerrar);

		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
