/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package ventana;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class Panel_SJF extends JPanel {

	// Componentes inicializados
	private JButton calcular = null;
	private JButton volver = null;

	private JLabel nfilas = null;
	private JTextField filas = null;
	private JButton generar = null;

	private JLabel labelprocesos = null;
	private JTable procesos = null;
	private JScrollPane tableContainer = null;
	private Vector<String> columnNames = null;
	private Vector<Object> rowData = null;

	private JLabel labelgantt = null;
	private JTextArea gantt = null;
	private JScrollPane textContainer = null;

	private JLabel esperaMed = null;
	private JLabel retornoMed = null;

	private Font font = null;

	private Vector<String> nombres = null;
	private Vector<Integer> llegada = null;
	private Vector<Integer> rafaga = null;
	private Vector<Integer> espera = null;
	private Vector<Integer> ganttValues = null;

	// SJF
	private Vector<Integer> revisados = null;

	private JLabel resultEspera = null;
	private JLabel resultRetorno = null;

	private JCheckBox expulsivo = null;
	private JCheckBox noexpulsivo = null;

	// Constructor
	public Panel_SJF(Frame_Principal frame) {

		// Configuraci�n
		this.setVisible(false);
		this.setSize(frame.getWIDTH(), frame.getHEIGHT());
		this.setLayout(null);
		this.setOpaque(false);

		// Componentes instanciados
		this.labelprocesos = new JLabel("Tabla de Procesos");
		this.labelprocesos.setBounds((int) (frame.getWIDTH() * 0.8) / 2, 10, 200, 25);
		this.add(this.labelprocesos);

		this.columnNames = new Vector<>();
		this.rowData = new Vector<>();
		this.llegada = new Vector<>();
		this.rafaga = new Vector<>();
		this.nombres = new Vector<>();
		this.espera = new Vector<>();
		this.ganttValues = new Vector<>();
		this.revisados = new Vector<>();

		this.columnNames.add("Proceso");
		this.columnNames.add("Tiempo de Llegada");
		this.columnNames.add("Tiempo de R�faga");

		this.procesos = new JTable(rowData, columnNames);
		this.procesos.getTableHeader().setReorderingAllowed(false);
		this.tableContainer = new JScrollPane(this.procesos);
		this.tableContainer.setBounds(10, 30, (int) (frame.getWIDTH() * 0.8), (frame.getHEIGHT() / 2) - 20);
		this.add(this.tableContainer);

		this.nfilas = new JLabel("N de Procesos");
		this.nfilas.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 10, 100, 30);
		this.add(nfilas);

		this.filas = new JTextField();
		this.filas.setBounds((int) (frame.getWIDTH() * 0.8) + 110, 10, 40, 25);
		this.add(this.filas);

		this.generar = new JButton("Generar Tabla");
		this.generar.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 40, 140, 25);
		this.generar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					if (Integer.parseInt(filas.getText()) < 1 || filas.getText() == null) {
						new Frame_Alerta(
								"En el campo de numero de filas" + "\nDebe ingresar un numero entero positivo");
						return;
					}

					int newRows = Integer.parseInt(filas.getText());
					setProcessTable(newRows);

				} catch (Exception e2) {
					// e2.printStackTrace();
					// e2.getMessage();
					new Frame_Alerta("Ingrese un numero entero positivo para generar tabla");
				}
			}
		});
		this.add(this.generar);

		this.resultEspera = new JLabel("0");
		this.resultEspera.setBounds(10 + 200, (int) (frame.getHEIGHT() / 1.8) + 100, 200, 30);
		this.add(this.resultEspera);

		this.resultRetorno = new JLabel("0");
		this.resultRetorno.setBounds(10 + 200, (int) (frame.getHEIGHT() / 1.8) + 150, 200, 30);
		this.add(this.resultRetorno);

		this.calcular = new JButton("Calcular");
		this.calcular.setBounds((int) (frame.getWIDTH() * 0.8) + 10, (frame.getHEIGHT() / 2) - 41, 140, 50);
		this.calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Manejo de excepciones
				if (procesos.getRowCount() < 1) {
					new Frame_Alerta("Antes de calcular \ndebe generar una tabla e ingresar los datos");
					return;
				}

				// Algoritmo
				try {

					for (int i = 0; i < procesos.getRowCount(); i++) {
						for (int j = 1; j < 3; j++) {
							if (Integer.parseInt("" + procesos.getValueAt(i, j)) < 0) {
								new Frame_Alerta("No est� permitido que se ingresen\n" + "valores negativos");
								return;
							}
						}
					}

					if (noexpulsivo.isSelected()) {
						// Limpia los datos antiguos
						nombres.clear();
						llegada.clear();
						rafaga.clear();
						ganttValues.clear();
						// Recolecta los datos de la tabla para transformarlos a
						// int
						for (int j = 0; j < procesos.getRowCount(); j++) {
							nombres.add("" + procesos.getValueAt(j, 0));
							llegada.add(Integer.parseInt("" + procesos.getValueAt(j, 1)));
							rafaga.add(Integer.parseInt("" + procesos.getValueAt(j, 2)));
						}

						// Limpia las posiciones antiguas
						espera.clear();// Posiciones de los procesos en espera
						revisados.clear();// Posiciones de los procesos ya
											// revisados
						int menorRafaga = 0;
						int tiempoTranscurrido = 0;
						int procesado = 0;

						for (int i = 0; i < rafaga.size(); i++) {

							// Procesos con tiempo de llegada menor al
							// transcurrido
							for (int j = 0; j < rafaga.size(); j++) {
								if (llegada.get(j) <= tiempoTranscurrido && !revisados.contains(j)
										&& !espera.contains(j)) {
									espera.add(j);
								}
							}

							if (espera.size() == 0) {// Espera a que se llene la
														// cola de espera
								tiempoTranscurrido++;
								i--;
							} else {
								menorRafaga = espera.get(0);
								procesado = 0;

								// Elijo el con menor rafaga
								for (int k = 1; k < espera.size(); k++) {
									if (rafaga.get(espera.get(k)) < rafaga.get(menorRafaga)) {
										menorRafaga = espera.get(k);
										procesado = k;
									}
								}

								revisados.add(menorRafaga);
								ganttValues.add(menorRafaga);
								tiempoTranscurrido += rafaga.get(menorRafaga);
								espera.remove(procesado);
							}
						}

						// Diagrama de Gantt

						String ganttText = "";
						int acumulado = llegada.get(ganttValues.get(0));
						Vector<Integer> valores = new Vector<>();
						valores.add(acumulado);
						ganttText += acumulado + "   ";
						ganttText += "[" + nombres.get(ganttValues.get(0)) + "]   ";

						for (int i = 0; i < ganttValues.size(); i++) {
							acumulado += rafaga.get(ganttValues.get(i));
							ganttText += acumulado + "   ";
							valores.add(acumulado);
							if (i < ganttValues.size() - 1) {
								ganttText += "[" + nombres.get(ganttValues.get(i + 1)) + "]   ";
							}
						}

						gantt.setText(ganttText);

						// Espera media y retorno medio

						float esperaMedio = 0;
						float retornoMedio = 0;

						for (int i = 0; i < llegada.size(); i++) {
							esperaMedio += (float) valores.get(i);
							esperaMedio -= (float) llegada.get(ganttValues.get(i));
						}

						esperaMedio = esperaMedio / llegada.size();

						for (int i = 1; i < valores.size(); i++) {
							retornoMedio += (float) valores.get(i);
						}

						retornoMedio = retornoMedio / llegada.size();

						resultEspera.setText("" + esperaMedio);
						resultRetorno.setText("" + retornoMedio);

						if ((retornoMedio < 0.0) || esperaMedio < 0.0) {
							new Frame_Alerta("El algoritmo ha dado valores negativos\n"
									+ "Esto quiere decir que este algoritmo\n" + "no es aplicable para este caso\n"
									+ "por lo tanto se debe elegir otro");
						}

					}
					////////////////////////////////////////////////////
					else {
						if (expulsivo.isSelected()) {

							nombres.clear();
							llegada.clear();
							rafaga.clear();
							ganttValues.clear();
							espera.clear();
							revisados.clear();

							Vector<Integer> jobs = new Vector<>();
							Vector<Integer> orden = new Vector<>();

							int menorRafaga = 0;
							int tiempoTranscurrido = 0;
							int procesado = 0;

							// Obtengo los datos de la tabla
							for (int j = 0; j < procesos.getRowCount(); j++) {
								nombres.add("" + procesos.getValueAt(j, 0));
								llegada.add(Integer.parseInt("" + procesos.getValueAt(j, 1)));
								rafaga.add(Integer.parseInt("" + procesos.getValueAt(j, 2)));
								jobs.add(Integer.parseInt("" + procesos.getValueAt(j, 2)));
							}

							while (!checkVector(jobs)) {
								// Deja en espera los procesos que entran
								for (int i = 0; i < rafaga.size(); i++) {
									if (llegada.get(i) <= tiempoTranscurrido && !revisados.contains(i)
											&& !espera.contains(i)) {
										espera.add(i);
									}
								}

								if (espera.size() > 0) {
									// Elijo el con menos rafaga
									menorRafaga = espera.get(0);
									procesado = 0;

									for (int i = 1; i < espera.size(); i++) {
										if (jobs.get(espera.get(i)) < jobs.get(menorRafaga)) {
											menorRafaga = espera.get(i);
											procesado = i;
										}
									}

									jobs.set(menorRafaga, jobs.get(menorRafaga) - 1);
									// System.out.println(jobs.get(menorRafaga));
									// System.out.println(nombres.get(menorRafaga));
									orden.add(menorRafaga);

									if (jobs.get(menorRafaga) == 0) {
										revisados.add(menorRafaga);
										espera.remove(procesado);
									}
								}

								tiempoTranscurrido++;

							}

							Vector<Integer> gantt = new Vector<>();
							Vector<Integer> valores = new Vector<>();

							int tMinimo = llegada.get(0);

							for (int i = 1; i < llegada.size(); i++) {
								if (llegada.get(i) < tMinimo) {
									tMinimo = llegada.get(i);
								}
							}
							// Calcular el tminimo sirve cuando
							// los procesos llegan despues del tiempo 0
							gantt.add(orden.get(0));
							valores.add(tMinimo);

							for (int i = 1; i < orden.size(); i++) {
								if (orden.get(i) != orden.get(i - 1)) {
									gantt.add(orden.get(i));
									valores.add(i + tMinimo);
								}
								if (i + 1 == orden.size()) {
									valores.add(i + 1 + tMinimo);
								}
							}

							String ganttText = "";
							ganttText += "" + valores.get(0);

							for (int i = 0; i < gantt.size(); i++) {
								// System.out.println(nombres.get(gantt.get(i)));
								// System.out.println(valores.get(i+1));

								ganttText += "   [" + nombres.get(gantt.get(i)) + "]   " + valores.get(i + 1);
							}

							Panel_SJF.this.gantt.setText(ganttText);

							// Tiempos
							// espera.clear();
							// Vector<Integer> espera = new Vector<>();

							Vector<Integer> values = new Vector<>();

							float espera = 0;
							float esperaMedia = 0;

							for (int i = 0; i < llegada.size(); i++) {
								for (int j = 0; j < gantt.size(); j++) {
									if (gantt.get(j) == i) {
										values.add(valores.get(j));
										values.add(valores.get(j + 1));
									}
								}

								for (int x = 0; x < values.size(); x++) {
									if (x == 0) {
										espera += values.get(x) - llegada.get(i);
									} else {
										if (values.size() > 2) {
											espera += values.get(x) - values.get(x - 1);
										}
									}
									x++;
								}
								values.clear();
								esperaMedia += espera;
								espera = 0;
							}

							esperaMedia = esperaMedia / llegada.size();
							resultEspera.setText("" + esperaMedia);

							float retornoMedio = 0;

							for (int j = 0; j < llegada.size(); j++) {
								for (int i = gantt.size() - 1; i >= 0; i--) {
									if (gantt.get(i) == j) {
										retornoMedio += valores.get(i + 1);
										break;
									}
								}
							}
							retornoMedio = retornoMedio / llegada.size();
							resultRetorno.setText("" + retornoMedio);

							if ((retornoMedio < 0.0) || esperaMedia < 0.0) {
								new Frame_Alerta("El algoritmo ha dado valores negativos\n"
										+ "Esto quiere decir que este algoritmo\n" + "no es aplicable para este caso\n"
										+ "por lo tanto se debe elegir otro");
							}

						}
					}
				} catch (Exception e2) {
					// e2.printStackTrace();
					// e2.getMessage();
					new Frame_Alerta("Debe ingresar los valores correctamente");
				}
			}
		});
		this.add(this.calcular);

		this.labelgantt = new JLabel("Diagrama de Gantt");
		this.labelgantt.setBounds((int) (frame.getWIDTH() * 0.8) / 2, (int) (frame.getHEIGHT() / 1.8) - 25, 200, 25);
		this.add(this.labelgantt);

		this.font = new Font("Verdana", Font.BOLD, 15);

		this.gantt = new JTextArea();
		this.gantt.setEditable(false);
		this.gantt.setFont(this.font);
		this.textContainer = new JScrollPane(this.gantt);
		this.textContainer.setBounds(10, (int) (frame.getHEIGHT() / 1.8), frame.getWIDTH() - 20, 100);
		this.add(this.textContainer);

		this.esperaMed = new JLabel("Tiempo medio de espera:");
		this.esperaMed.setBounds(10, (int) (frame.getHEIGHT() / 1.8) + 100, 200, 30);
		this.add(this.esperaMed);

		this.retornoMed = new JLabel("Tiempo medio de retorno:");
		this.retornoMed.setBounds(10, (int) (frame.getHEIGHT() / 1.8) + 150, 200, 30);
		this.add(this.retornoMed);

		this.expulsivo = new JCheckBox("Expulsivo");
		this.expulsivo.setSelected(true);
		this.expulsivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				noexpulsivo.setSelected(false);

				if (!expulsivo.isSelected()) {
					noexpulsivo.setSelected(true);
				}
			}
		});
		this.expulsivo.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 80, 100, 25);
		this.add(this.expulsivo);

		this.noexpulsivo = new JCheckBox("No-Expulsivo");
		this.noexpulsivo.setSelected(false);
		this.noexpulsivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				expulsivo.setSelected(false);

				if (!noexpulsivo.isSelected()) {
					expulsivo.setSelected(true);
				}
			}
		});
		this.noexpulsivo.setBounds((int) (frame.getWIDTH() * 0.8) + 10, 120, 100, 25);
		this.add(this.noexpulsivo);

		this.volver = new JButton("Volver");
		this.volver.setBounds(1, frame.getHEIGHT() - 60, 100, 30);
		this.volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.ps.setVisible(false);
				frame.setTitle("Planificador de procesos");
				frame.pp.setVisible(true);
			}
		});
		this.add(this.volver);
	}

	/* M�todos */
	// Limpia la tabla y a�ade nuevas filas
	private void setRowNumber(int rows) {
		this.rowData.clear();
		for (int i = 0; i < rows; i++) {
			this.rowData.addElement(null);
		}
	}

	// Limpia el modelo de la tabla (datos y columnas) y colca el nuevo
	private void setProcessTable(int rows) {
		this.setRowNumber(rows);
		this.procesos.setModel(new DefaultTableModel(this.rowData, this.columnNames));
		// Nombrar los procesos
		for (int i = 0; i < procesos.getRowCount(); i++) {
			procesos.setValueAt("P" + (i + 1), i, 0);
		}
	}

	// Revisa el vector si todos sus valores son 0
	private boolean checkVector(Vector<Integer> v) {
		int count = 0;

		for (int i = 0; i < v.size(); i++) {
			if (v.get(i) == 0) {
				count++;
			}
		}

		if (count == v.size()) {
			return true;
		}

		return false;
	}
}
