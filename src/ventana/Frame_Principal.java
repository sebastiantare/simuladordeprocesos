/*
 * Universidad del B�o-B�o (2015)
 * Asignatura: Sistemas Operativos
 * Profesor: Marco Iturra Mella
 * 
 * Por: Sebasti�n Tar� Bustos
 * Carrera: IECI
 * Correos: stare@alumnos.ubiobio.cl, deathshadow_4@hotmail.com
 */
package ventana;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Frame_Principal extends JFrame {
	// Atributos
	private final int WIDTH = 800;
	private final int HEIGHT = 600;

	// Paneles inicializados
	public Panel_Principal pp = null;
	public Panel_FIFO pf = null;
	public Panel_SJF ps = null;
	public Panel_RR pr = null;
	public Panel_PRIOR prr = null;

	// Constructor
	public Frame_Principal() {
		// Configuraci�n
		this.setSize(this.WIDTH, this.HEIGHT);
		this.setTitle("Planificador de procesos");
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(null);

		// Paneles instanciados
		this.pp = new Panel_Principal(this);
		this.add(this.pp);

		this.pf = new Panel_FIFO(this);
		this.add(this.pf);

		this.ps = new Panel_SJF(this);
		this.add(this.ps);

		this.pr = new Panel_RR(this);
		this.add(this.pr);

		this.prr = new Panel_PRIOR(this);
		this.add(this.prr);

		// Configuraci�n
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// M�todos de la clase
	// Estos m�todos son algo reduntantes
	// Ya que la clase en s� posee m�todos
	// Para obtener estos datos
	public int getWIDTH() {
		return WIDTH;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}
}
